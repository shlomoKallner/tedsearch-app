FROM maven:3.6.3-jdk-8

COPY pom.xml application.properties /usr/src/mymaven/
COPY ./src/main/java/ /usr/src/mymaven/src/main/java/
COPY ./src/test/ /usr/src/mymaven/src/test/
COPY ./src/main/resources/application.properties ./src/main/resources/*.xml /usr/src/mymaven/src/main/resources/
COPY application.properties entry-point.sh /usr/src/mymaven/app/
WORKDIR /usr/src/mymaven

VOLUME [ "/usr/src/mymaven/target" ]

# build the app
# RUN mvn -Dmaven.test.skip -DskipTests verify 
ENTRYPOINT [ "mvn", "verify" ]