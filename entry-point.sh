#! /bin/bash -ex

## usage call with bash at the docker ENTRYPOINT with no arguments.

### MAIN ###

if [[ $(pwd) != "app" ]]; then
    cd /app
fi

# grant execute access to the .jar file[s]..
chmod +x ./*.jar


### WRITE NETCAT USAGE HERE!

# now let's run this/these jar!
java -jar ./*.jar --spring.config.location=./application.properties